# Mamba X3 #

Mamba is an amazing commnad line which you can download many apps and ALSO use many commands.

### Commands List ###

* Check out com.txt to find commands.

### How do I get set up? ###

* Download Project
* Unzip Project
* Open com.txt to find commands
* Open main.py to run cmd.
* Advanced features instructions are below.

### Advanced: ###

```
Python 3 For Windows:
$$ mamba get python -v 3 -win
Python 3 For Mac:
$$ mamba get python -v 3 -mac
CoreV.SDK for Windows:
$$ mamba package get corev -v 2 -win 
CoreV.SDK for Mac:
$$ mamba package get corev -v 2 -win -chip uni
```
### By: ###

* Admiral TM, Inc.